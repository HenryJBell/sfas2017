﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class FlockWithGroup : MonoBehaviour
{
    [SerializeField]
    private GroupTag.Group GroupCode;

    [SerializeField]
    private float Force = 0.0f;

    [SerializeField]
    private float BuddyDistance = 100.0f;

    [SerializeField]
    private float AvoidDistance = 1.0f;

    [SerializeField]
    private float CheckForBuddiesInterval = 10.0f;

    private List<GroupTag> mCurrentBuddies = new List<GroupTag>();
    private Rigidbody mBody;
    private float mCountDownToCheck = 0.0f;
    private bool mUpdateBuddiesNextFrame = false;


    //-----------------------------------------------------Unity Functions-----------------------------------------------------

    void Awake()
    {
        mBody = GetComponent<Rigidbody>();
        Enemy.EnemyDied += Enemy_EnemyDied;
    }

    void Update()
    {
        // Updating the buddy list at regular intervals.
        mCountDownToCheck -= Time.deltaTime;
        if (mCountDownToCheck <= 0.0f || mUpdateBuddiesNextFrame)
        {
            UpdateBuddyList();
            mCountDownToCheck = CheckForBuddiesInterval;
            mUpdateBuddiesNextFrame = false;
        }

        // Flocking with any nearby buddies.
        FlockWithBuddies();
    }


    //-----------------------------------------------------Private Functions-----------------------------------------------------

    // Update the the 'mCurrentBuddies' list.
    private void UpdateBuddyList()
    {
        mCurrentBuddies.Clear();

        GroupTag[] individuals = FindObjectsOfType<GroupTag>();

        for (int count = 0; count < individuals.Length; ++count)
        {
            if (individuals[count].gameObject != gameObject && individuals[count].Affiliation == GroupCode )
            {
                Vector3 difference = individuals[count].transform.position - transform.position;

                // Adding to the buddy list if they are close enough and not already included in it.
                if (difference.magnitude <= BuddyDistance)
                    if (!mCurrentBuddies.Contains(individuals[count]))
                        mCurrentBuddies.Add(individuals[count]);
            }
        }
    }

    // Flock with any nearby buddies.
    private void FlockWithBuddies()
    {
        if (mCurrentBuddies.Count > 0)
        {
            Vector3 align = Vector3.zero;
            Vector3 cohesion = Vector3.zero; 
            Vector3 avoid = Vector3.zero;
            
            // Adjusting the align, cohesion and avoid vectors based on the properties of the buddies.
            for (int count = 0; count < mCurrentBuddies.Count; ++count)
            {
                Rigidbody body = mCurrentBuddies[count].GetComponent<Rigidbody>();
                align += body.velocity;
                cohesion += body.transform.position;
                if ( ( body.transform.position - transform.position ).magnitude < AvoidDistance)
                {
                    avoid += body.transform.position;
                }
            }

            align /= mCurrentBuddies.Count;
            cohesion /= mCurrentBuddies.Count;
            avoid /= mCurrentBuddies.Count;

            align.Normalize();
            cohesion = cohesion - transform.position;
            cohesion.Normalize();
            avoid = transform.position - avoid;
            avoid.Normalize();

            mBody.AddForce(( align + cohesion + avoid) * Force * Time.deltaTime);
        }
    }

    private void Enemy_EnemyDied()
    {
        // Updating the buddy list if an enemy dies.
        mUpdateBuddiesNextFrame = true;
    }
}
