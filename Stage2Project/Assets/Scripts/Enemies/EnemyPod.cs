﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class EnemyPod : Character {

    [SerializeField]
    private Enemy EnemyPrefab;

    [SerializeField]
    private GameObject SpawnParticle;

    private float mSpawnDelay = 1.0f;
    private bool mEnemySpawned = false;
    private GameManager mGameManager;


    //-----------------------------------------------------Unity Functions-----------------------------------------------------

    void Awake()
    {
        mGameManager = FindObjectOfType<GameManager>();
    }


    //-----------------------------------------------------Public Functions-----------------------------------------------------

    // Spawn the enemy after a delay of 'spawnDelay' seconds.
    public void StartSpawnProcess(float spawnDelay)
    {
        mSpawnDelay = Mathf.Max(0.0f, spawnDelay);
        StartCoroutine(SpawnEnemyAfterDelay());
    }


    //-----------------------------------------------------Private Functions-----------------------------------------------------

    // Call 'SpawnEnemy()' after 'mSpawnDelay' seconds.
    private IEnumerator SpawnEnemyAfterDelay()
    {
        yield return new WaitForSeconds(mSpawnDelay);
        SpawnEnemy();
    }

    // Spawn the 'EnemyPrefab' and 'SpawnParticle' and destroy the gameobject.
    private void SpawnEnemy()
    {
        if (mEnemySpawned) return;

        // Spawning the 'EnemyPrefab'.
        var obj = Instantiate(EnemyPrefab).gameObject;
        mGameManager.AddToObjectsList(obj);
        obj.transform.position = new Vector3(transform.position.x, obj.transform.position.y, transform.position.z);
        var randomPosComponent = obj.GetComponent<RandomStartPosition>();
        if (randomPosComponent != null)
            randomPosComponent.enabled = false;
        
        // Spawning the 'SpawnParticle'
        var particle = Instantiate(SpawnParticle, transform.position + Vector3.up * 0.5f, SpawnParticle.transform.rotation) as GameObject;
        mGameManager.AddToObjectsList(particle);

        // Destroying the gameobject.
        Destroy(this.gameObject);

        mEnemySpawned = true;
    }


    //-----------------------------------------------------Protected Functions-----------------------------------------------------

    // Call the 'SpawnEnemy()' function.
    protected override void Die()
    {
        SpawnEnemy();
    }
}
