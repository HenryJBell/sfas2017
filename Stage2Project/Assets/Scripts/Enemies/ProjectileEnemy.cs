﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileEnemy : Enemy {

    [SerializeField]
    private float RotSpeed = 10.0f;

    private Player mPlayer;


    //-----------------------------------------------------Unity Functions-----------------------------------------------------

    protected override void Start()
    {
        base.Start();
        mPlayer = FindObjectOfType<Player>();
    }

    void Update()
    {
        // Smoothly rotating to face the player and activate weapons.
        if (mPlayer != null && Time.timeScale > 0)
        {
            var targetRot = Quaternion.LookRotation(mPlayer.transform.position - transform.position, Vector3.up);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRot, RotSpeed * Time.deltaTime);
            ActivateAllWeapons();
        }
    }
}
