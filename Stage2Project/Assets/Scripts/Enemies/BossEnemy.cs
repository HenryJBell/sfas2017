﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossEnemy : ProjectileEnemy {

    [SerializeField]
    private List<GameObject> HealthIndicators;

    private int mInitialHealth;


    //-----------------------------------------------------Unity Functions-----------------------------------------------------

    protected override void Start()
    {
        base.Start();
        mInitialHealth = Health;
    }


    //-----------------------------------------------------Public Functions-----------------------------------------------------

    // Apply damage to the boss enemy.
    public override void TakeDamage(int damage)
    {
        base.TakeDamage(damage);

        // Adjusting the number of visible health indicators on the boss enemy's back to reflect their remaining health.
        var healthFraction = (float)Health / (float)mInitialHealth;
        int maxHealthIndicatorIndex = Mathf.Min(Mathf.FloorToInt((1.0f - healthFraction) * (HealthIndicators.Count + 1)), HealthIndicators.Count);
        for (int i = 0; i < maxHealthIndicatorIndex; i++)
            HealthIndicators[i].SetActive(false);
    }
}
