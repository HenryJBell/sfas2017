﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Character {

    public delegate void GameEvent();
    public static event GameEvent EnemyDied;


    //----------------------------------------------------Public Functions---------------------------------------------------

    // Apply damage to the enemy.
    public override void TakeDamage(int damage)
    {
        FindObjectOfType<ScoreManager>().IncrementScore(1);
        base.TakeDamage(damage);        
    }

    //----------------------------------------------------Protected Functions---------------------------------------------------

    // Kill the enemy.
    protected override void Die()
    {
        FindObjectOfType<ScoreManager>().IncrementMultiplier(1);
        if (EnemyDied != null)
            EnemyDied();
        base.Die();
    }
}
