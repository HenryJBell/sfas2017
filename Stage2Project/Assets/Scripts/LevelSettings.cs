﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LevelSettings
{
    public int EnemyPodCount = 0;
    public int TimeBetweenEnemyPods = 2;
    public int BossPodCount = 0;
    public int TimeBetweenBossPods = 2;
    public int DecalCount = 1;
    public bool SpawnLevelExit = true;
}
