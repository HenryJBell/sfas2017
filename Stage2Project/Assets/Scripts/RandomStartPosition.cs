﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomStartPosition : MonoBehaviour
{
    [SerializeField]
    [Range(0.0f, 1.0f)]
    private float XMultiplier = 1.0f;

    [SerializeField]
    [Range(0.0f, 1.0f)]
    private float YMultiplier = 1.0f;

    [SerializeField]
    private bool RandomRot = false;


    //-----------------------------------------------------Unity Functions-----------------------------------------------------

    void Start()
    {
        // Randomising the position of the object within the arena bounds.
        float hw = Arena.Width * 0.5f * XMultiplier;
        float hh = Arena.Height * 0.5f * YMultiplier;
        float x = Random.Range( -hw, hw );
        float z = Random.Range(-hh, hh);
        transform.position = new Vector3(x, transform.position.y, z);

        // Randomizing the Y rotation of the object if 'RandomRot' is true.
        if (RandomRot) transform.rotation = Quaternion.Euler(transform.eulerAngles.x, Random.Range(0, 360), transform.eulerAngles.z);
    }
}
