﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WrapPosition : MonoBehaviour
{
    [SerializeField]
    private float Multiplier = 1.0f;


    //-----------------------------------------------------Unity Functions-----------------------------------------------------

    void LateUpdate ()
    {
        Vector3 position = transform.position;

        if (position.x < Arena.Width * -0.5f * Multiplier) position.x += Arena.Width * Multiplier;
        else if (position.x > Arena.Width * 0.5f * Multiplier) position.x -= Arena.Width * Multiplier;

        if (position.z < Arena.Height * -0.5f * Multiplier) position.z += Arena.Height * Multiplier;
        else if (position.z > Arena.Height * 0.5f * Multiplier) position.z -= Arena.Height * Multiplier;

        transform.position = position;
    }
}
