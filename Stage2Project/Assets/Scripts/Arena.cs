﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Arena : MonoBehaviour
{
    [SerializeField]
    private Camera Cam;

    [SerializeField]
    private Vector2 SizeMultiplier = Vector2.one;

    [SerializeField]
    private float WallHeight = 1.0f;

    [SerializeField]
    private List<Transform> Walls = new List<Transform>();

    public static float Width { get; private set; }
    public static float Height { get; private set; }


    //-----------------------------------------------------Unity Functions-----------------------------------------------------

    void OnEnable()
    {
        if (!Application.isPlaying)
            Calculate();
    }

    void Update()
    {
        // Recalculating the arena plane if in the Unity Editor and not playing.
        #if UNITY_EDITOR 

        if (!Application.isPlaying)
            Calculate();

        #endif
    }


    //-----------------------------------------------------Public Functions-----------------------------------------------------

    // Update the size of the plane to match the size of the camera frustrum at the far clipping plane.
    public void Calculate()
    {
        if (Cam != null)
        {
            Height = CameraUtils.FrustumHeightAtDistance(Cam.farClipPlane - 1.0f, Cam.fieldOfView);
            Width = Height * Cam.aspect;

            Height *= SizeMultiplier.y;
            Width *= SizeMultiplier.x;

            transform.localScale = new Vector3(Width * 0.1f, 1.0f, Height * 0.1f);

            UpdateWalls();
        }
    }


    //-----------------------------------------------------Private Functions-----------------------------------------------------

    // Position the walls at the edge of the arena and scale them appropriately.
    private void UpdateWalls()
    {
        List<Vector3> wallOffsets = new List<Vector3>();
        wallOffsets.Add(new Vector3(Width / 2, WallHeight / 2, 0));
        wallOffsets.Add(new Vector3(-Width / 2, WallHeight / 2, 0));
        wallOffsets.Add(new Vector3(0, WallHeight / 2, Height / 2));
        wallOffsets.Add(new Vector3(0, WallHeight / 2, -Height / 2));

        List<Vector3> wallScales = new List<Vector3>();
        wallScales.Add(new Vector3(1, 1, Height));
        wallScales.Add(new Vector3(1, 1, Height));
        wallScales.Add(new Vector3(Width, 1, 1));
        wallScales.Add(new Vector3(Width, 1, 1));

        for (int i = 0; i < Walls.Count && i < 4; i++)
        {
            Walls[i].position = transform.position + wallOffsets[i];
            Walls[i].localScale = new Vector3(wallScales[i].x / transform.localScale.x, WallHeight / transform.localScale.y, wallScales[i].z / transform.localScale.z);
        }
    }
}
