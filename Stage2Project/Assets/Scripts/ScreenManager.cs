﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenManager : MonoBehaviour {

    public enum Screens { TitleScreen, GameScreen, BuffScreen, WinScreen, LooseScreen, NumScreens }

    public delegate void GameEvent();
    public static event GameEvent OnNewGame;

    [SerializeField]
    private Texture2D CursorTexture;

    [SerializeField]
    private float InitialFadeInDuration = 2.0f;

    [SerializeField]
    private float StartNewGameDelay = 0.0f;

    [SerializeField]
    private float QuitGameDelay = 0.0f;

    [SerializeField]
    private List<Text> HealthUI;

    [SerializeField]
    private List<Text> ScoreUI;

    [SerializeField]
    private List<Text> HighScoreUI;

    [SerializeField]
    private Image ScreenFaderImage;

    private Canvas [] mScreens;
    private Screens mCurrentScreen;
    private float mScreenFade;
    private bool mButtonsLocked = false;


    //-----------------------------------------------------Unity Functions-----------------------------------------------------

    void Awake()
    {
        // Initialising the 'mScreens' canvas array.
        mScreens = new Canvas[(int)Screens.NumScreens];

        // Getting all canvases from the game objects children.
        Canvas[] screens = GetComponentsInChildren<Canvas>();

        // Adding the canvases from the scene into their appropriate slots in the 'mScreens' array.
        for (int count = 0; count < screens.Length; ++count)
        {
            for (int slot = 0; slot < mScreens.Length; ++slot)
            {
                if (mScreens[slot] == null && ((Screens)slot).ToString() == screens[count].name)
                {
                    mScreens[slot] = screens[count];
                    break;
                }
            }
        }

        // Disabling all screens but the title screen.
        for (int screen = 1; screen < mScreens.Length; ++screen)
            mScreens[screen].enabled = false;
        mCurrentScreen = Screens.TitleScreen;

        // Fading in the game.
        FadeIn(InitialFadeInDuration);

        // Setting the cursor to be the standard mouse.
        EnableCrossHair(false);
    }

    void Update()
    {
        // Updating the alpha of the screen fader based on 'mScreenFade'.
        var screenFaderCol = ScreenFaderImage.color;
        screenFaderCol.a = Mathf.Clamp01(screenFaderCol.a + mScreenFade * Time.deltaTime);
        ScreenFaderImage.color = screenFaderCol;
        ScreenFaderImage.enabled = (screenFaderCol.a > 0);
    }


    //-----------------------------------------------------Public Functions-----------------------------------------------------

    // Start a new game at level 0 after fading out the current menu.
    public void StartNewGame()
    {
        if (mButtonsLocked) return;
        mButtonsLocked = true;
        StartCoroutine(StartNewGameAfterDelay());
        FadeOut(StartNewGameDelay);
    }

    // Start a new game at level 0 immediately.
    public void StartNewGameImmediate()
    {
        if(OnNewGame != null)
            OnNewGame();

        TransitionTo(Screens.GameScreen);
        mButtonsLocked = false;
    }

    // Reset the high score to 0.
    public void ResetHighScore()
    {
        var scoreManager = FindObjectOfType<ScoreManager>();
        if(scoreManager != null) scoreManager.ResetHighScore();
    }

    // Exit the application after fading out the game.
    public void QuitGame()
    {
        if (mButtonsLocked) return;
        mButtonsLocked = true;
        StartCoroutine(QuitGameAfterDelay());
        FadeOut(QuitGameDelay);
    }

    // Update the player health UI elements.
    public void UpdatePlayerHealth(int health)
    {
        health = Mathf.Max(health, 0);
        foreach(var healthUI in HealthUI)
        {
            healthUI.text = "Health: " + health;
            healthUI.color = (health <= 2) ? Color.red : Color.white;
        }        
    }

    // Update the score UI elements.
    public void UpdateScore(string score)
    {
        foreach(var scoreUI in ScoreUI)
            scoreUI.text = "Score: " + score;
    }

    // Update the high score UI elements.
    public void UpdateHighScore(int highScore)
    {
        foreach (var highScoreUI in HighScoreUI)
            highScoreUI.text = "High Score: " + highScore;
    }

    // Disable the current screen and transition to 'screen'.
    public void TransitionTo(Screens screen)
    {
        mScreens[(int)mCurrentScreen].enabled = false;
        mScreens[(int)screen].enabled = true;
        mCurrentScreen = screen;
    }

    // Fade to black over 'duration' seconds.
    public void FadeOut(float duration)
    {
        if (duration <= 0) duration = 0.0000001f;
        mScreenFade = 1 / duration;
    }

    // Fade in over 'duration' seconds.
    public void FadeIn(float duration)
    {
        if (duration <= 0) duration = 0.0000001f;
        mScreenFade = 1 / -duration;
    }

    // Set the cursor to either be the default cursor or the cross hair.
    public void EnableCrossHair(bool enable)
    {
        Cursor.SetCursor(enable ? CursorTexture : null, new Vector2(CursorTexture.width/2, CursorTexture.height/2), CursorMode.Auto);
    }


    //-----------------------------------------------------Private Functions-----------------------------------------------------

    // Call 'StartNewGameImmediate()' after 'StartNewGameDelay' seconds.
    private IEnumerator StartNewGameAfterDelay()
    {
        yield return new WaitForSeconds(StartNewGameDelay);
        StartNewGameImmediate();
    }

    // Quit the application or exit playmode after 'QuitGameDelay' seconds.
    private IEnumerator QuitGameAfterDelay()
    {
        yield return new WaitForSeconds(QuitGameDelay);
        Application.Quit();
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #endif
    }
}
