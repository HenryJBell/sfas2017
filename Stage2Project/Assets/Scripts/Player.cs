﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Player : Character
{
    public enum WeaponConfig { Default, Dual, Shotgun }
    public enum PlayerState { Playing, ExitingLevel }

    public delegate void GameEvent();
    public static event GameEvent PlayerDied;

    public PlayerState State = PlayerState.Playing;

    [SerializeField]
    private float Acceleration;

    [SerializeField]
    private float MaxSpeed;

    [SerializeField]
    [Range(0.0f, 1.0f)]
    private float ExitLevelSpeedMultiplier = 1.0f;

    [SerializeField]
    private List<Weapon> DefaultWeapons = new List<Weapon>();

    [SerializeField]
    private List<Weapon> DualWeapons = new List<Weapon>();

    [SerializeField]
    private List<Weapon> ShotgunWeapons = new List<Weapon>();

    private Rigidbody mBody;
    private ScreenManager mScreenManager;


    //-----------------------------------------------------Unity Functions-----------------------------------------------------

    void Awake()
    {
        mBody = GetComponent<Rigidbody>();
        SetWeaponConfig(WeaponConfig.Default);
        mScreenManager = FindObjectOfType<ScreenManager>();
        UpdateHUD();        
    }

    void Update()
    {
        switch(State)
        {
            // Playing.
            case PlayerState.Playing:

                // Player movement.
                Vector3 moveDir = Vector3.zero;
                if (Input.GetKey(KeyCode.A)) moveDir.x--;
                if (Input.GetKey(KeyCode.D)) moveDir.x++;
                if (Input.GetKey(KeyCode.W)) moveDir.z++;
                if (Input.GetKey(KeyCode.S)) moveDir.z--;
                Accelerate(moveDir);

                // Player rotation.
                if(Time.timeScale >0)
                {
                    RotateToFaceMouse();
                    if (Input.GetMouseButton(0)) ActivateAllWeapons();
                }                    
                break;

            // Exiting Level.
            case PlayerState.ExitingLevel:

                // Moving the player towards the level exit.
                var exit = FindObjectOfType<LevelExit>();
                if(exit != null)
                    Accelerate(exit.transform.position - transform.position, ExitLevelSpeedMultiplier);               
                break;
        }
    }


    //----------------------------------------------------Public Functions----------------------------------------------------

    // Apply damage to the player.
    public override void TakeDamage(int damage)
    {
        base.TakeDamage(damage);
        UpdateHUD();
    }

    // Add health to the character.
    public override void Heal(int value)
    {
        base.Heal(value);
        UpdateHUD();
    }

    // Set the current weapon configuration being used by the player to 'config'.
    public void SetWeaponConfig(WeaponConfig config)
    {
        foreach (var weapon in DefaultWeapons)
            weapon.gameObject.SetActive(config == WeaponConfig.Default);
        foreach (var weapon in DualWeapons)
            weapon.gameObject.SetActive(config == WeaponConfig.Dual);
        foreach (var weapon in ShotgunWeapons)
            weapon.gameObject.SetActive(config == WeaponConfig.Shotgun);

        UpdateWeapons();
    }


    //----------------------------------------------------Private Functions----------------------------------------------------

    // Accelerate smoothly by adding force to the player rigidbody.
    private void Accelerate(Vector3 dir, float multiplier = 1.0f)
    {
        var maxForce = ((MaxSpeed - mBody.velocity.magnitude) * mBody.mass) / Time.deltaTime;
        var forceToAdd = Mathf.Min(Acceleration * multiplier * Time.deltaTime, maxForce);
        mBody.AddForce(dir.normalized * forceToAdd);
    }

    // Rotate the player to the mouse world position.
    private void RotateToFaceMouse()
    {
        // Converting the mouse screen position to a ray.
        var mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        // Creating a plane at the players location.
        var playerPlane = new Plane(Vector3.up, this.transform.position);

        // Rotating the player to face the point where the ray intersects the plane.
        float distance = 0;
        if (playerPlane.Raycast(mouseRay, out distance))
        {
            var target = mouseRay.GetPoint(distance);
            this.transform.rotation = Quaternion.LookRotation(target - this.transform.position);
        }
    }

    // Update the health HUD.
    private void UpdateHUD()
    {
        mScreenManager.UpdatePlayerHealth(Health);
    }


    //----------------------------------------------------Protected Functions---------------------------------------------------

    // Kill the player.
    protected override void Die()
    {
        if (PlayerDied != null)
            PlayerDied();
        base.Die();
    }
}
