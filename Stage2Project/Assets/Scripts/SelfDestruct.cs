﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestruct : MonoBehaviour {

    [SerializeField]
    private float Delay = 1.0f;


    //-----------------------------------------------------Unity Functions-----------------------------------------------------

    void Start()
    {
        StartCoroutine(SelfDestructCountdown());
    }


    //-----------------------------------------------------Private Functions---------------------------------------------------

    // Destroy this gameobject after 'Delay' seconds.
    private IEnumerator SelfDestructCountdown()
    {
        yield return new WaitForSeconds(Delay);
        Destroy(this.gameObject);
    }
}
