﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Faction { Player, Enemy }

[RequireComponent(typeof(Collider))]
public abstract class Character : MonoBehaviour {

    public Faction Faction;

    [SerializeField]
    protected int Health = 1;

    [SerializeField]
    private GameObject DeathParticles;

    private List<Weapon> mWeapons = new List<Weapon>();


    //------------------------------------------------------Unity Functions-----------------------------------------------------

    protected virtual void Start()
    {
        UpdateWeapons();
    }


    //-----------------------------------------------------Public Functions----------------------------------------------------

    // Apply damage to the character.
    public virtual void TakeDamage(int damage)
    {
        Health = Mathf.Max(Health - damage, 0);
        if (Health <= 0)
            Die();
    }

    // Add health to the character.
    public virtual void Heal(int value)
    {
        Health += value;
    }


    //----------------------------------------------------Protected Functions---------------------------------------------------

    // Activate the weapon in the 'mWeapons' list with index 'index'.
    protected virtual void ActivateWeapon(int index)
    {
        index = Mathf.Max(0, index);
        if (index >= mWeapons.Count) return;
        mWeapons[index].Activate(Faction);
    }

    // Activate all weapons in the 'mWeapons' list.
    protected virtual void ActivateAllWeapons()
    {
        for (int i = 0; i < mWeapons.Count; i++)
            ActivateWeapon(i);
    }

    // Update the 'mWeapons' list with all weapons that are children of the character.
    protected void UpdateWeapons()
    {
        mWeapons.Clear();
        var weapons = GetComponentsInChildren<Weapon>();
        for (int i = 0; i < weapons.Length; i++)
            mWeapons.Add(weapons[i]);
    }

    // Kill the character.
    protected virtual void Die()
    {
        var gameManager = FindObjectOfType<GameManager>();
        if (DeathParticles != null && gameManager != null)
            gameManager.AddToObjectsList(Instantiate(DeathParticles, transform.position, DeathParticles.transform.rotation));
        Destroy(gameObject);
    }
}
