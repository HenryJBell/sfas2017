﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour {

    [SerializeField]
    private float TimeBetweenMultiplierDecreases = 1.0f;

    private int mScore = 0;
    private int mMultiplier = 1;
    private float mTimeUntilNextMultiplierDecrease = 0.0f;
    private ScreenManager mScreenManager;


    //-----------------------------------------------------Unity Functions-----------------------------------------------------

    void Awake()
    {
        mScreenManager = FindObjectOfType<ScreenManager>();
        Reset();
    }

    void Update()
    {
        // Decreasing the score multiplier by 1 at regular intervals.
        mTimeUntilNextMultiplierDecrease -= Time.deltaTime;
        if(mTimeUntilNextMultiplierDecrease <= 0.0f)
        {
            IncrementMultiplier(-1);
            mTimeUntilNextMultiplierDecrease = TimeBetweenMultiplierDecreases;
        }
    }


    //-----------------------------------------------------Public Functions-----------------------------------------------------

    // Reset the score.
    public void Reset()
    {
        mScore = 0;
        mMultiplier = 1;
        UpdateUI();
    }

    // Increment the score by 'amount'.
    public void IncrementScore(int amount)
    {
        mScore += Mathf.Max(0, amount);
        UpdateUI();
    }

    // Increment the score multiplier by 'amount'.
    public void IncrementMultiplier(int amount)
    {
        mMultiplier = Mathf.Max(1, mMultiplier + amount);
        UpdateUI();
        mTimeUntilNextMultiplierDecrease = TimeBetweenMultiplierDecreases;
    }

    // Apply the multiplier to the score and reset it to 1.
    public void ApplyMultiplier()
    {
        mScore *= mMultiplier;
        mMultiplier = 1;
        UpdateHighScore();
        UpdateUI();
    }

    // Reset the highscore to 0.
    public void ResetHighScore()
    {
        PlayerPrefs.SetInt("HighScore", 0);
        UpdateUI();
    }


    //-----------------------------------------------------Private Functions-----------------------------------------------------

    // Set the highscore to 'mScore' if 'mScore' is greater than it.
    private void UpdateHighScore()
    {
        int highscore = PlayerPrefs.GetInt("HighScore");
        if (mScore > highscore)
            PlayerPrefs.SetInt("HighScore", mScore);
    }

    // Update the score and highscore UI elements.
    private void UpdateUI()
    {
        string text = mScore + (mMultiplier > 1 ? " x" + mMultiplier : "");
        mScreenManager.UpdateScore(text);
        mScreenManager.UpdateHighScore(PlayerPrefs.GetInt("HighScore"));
    }
}
