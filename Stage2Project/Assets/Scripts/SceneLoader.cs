﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour 
{
	[SerializeField]
    private string [] Scenes;

	public bool Loading { get; private set; }

	private WaitForEndOfFrame mWaitForEndOfFrame;


    //-----------------------------------------------------Unity Functions-----------------------------------------------------

    void Awake()
	{
		mWaitForEndOfFrame = new WaitForEndOfFrame();
        Loading = true;
        StartCoroutine(LoadScenes());
    }


    //-----------------------------------------------------Private Functions-----------------------------------------------------

    private IEnumerator LoadScenes()
	{
        // Loading the scenes from the scenes array.
		for( int count = 0; count < Scenes.Length; count++ )
		{
            AsyncOperation ao = SceneManager.LoadSceneAsync(Scenes[count], LoadSceneMode.Additive);
			if( ao != null )
				while( !ao.isDone )
					yield return mWaitForEndOfFrame;
		}
		yield return mWaitForEndOfFrame;
		Loading = false;
	}
}
