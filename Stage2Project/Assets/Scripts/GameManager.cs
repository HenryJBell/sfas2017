﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public enum State { Paused, Playing }

    [SerializeField]
    private int LevelIndex = 0;

    [SerializeField]
    private List<LevelSettings> LevelSettings;

    [SerializeField]
    private GameObject EnemyPrefab;

    [SerializeField]
    private EnemyPod EnemyPodPrefab;

    [SerializeField]
    private GameObject BossEnemyPrefab;

    [SerializeField]
    private EnemyPod BossEnemyPodPrefab;

    [SerializeField]
    private Player PlayerPrefab;

    [SerializeField]
    private LevelExit LevelExitPrefab;

    [SerializeField]
    private GameObject DecalPrefab;

    [SerializeField]
    private float StartNextLevelDelay = 0.0f;

    [SerializeField]
    private Arena Arena;

    private List<GameObject> mObjects;
    private Player mPlayer;
    private State mState;
    private ScreenManager mScreenManager;
    private ScoreManager mScoreManager;
    private BuffManager mBuffManager;


    //-----------------------------------------------------Unity Functions-----------------------------------------------------

    void Awake()
    {
        mScreenManager = FindObjectOfType<ScreenManager>();
        mScoreManager = FindObjectOfType<ScoreManager>();
        mBuffManager = FindObjectOfType<BuffManager>();

        ScreenManager.OnNewGame += ScreenManager_OnNewGame;
        Enemy.EnemyDied += Enemy_EnemyDied;
        Player.PlayerDied += Player_PlayerDied;
    }


    //-----------------------------------------------------Public Functions-----------------------------------------------------

    // Start a new game at level 0.
    public void StartNewGame()
    {
        LevelIndex = 0;
        BuildCurrentLevel(false);
        mScoreManager.Reset();
        SetState(State.Playing);
    }

    // Proceed to the next level after fading out the current level.
    public void StartNextLevel()
    {
        StartCoroutine(StartNextLevelAfterDelay());
        mScreenManager.FadeOut(StartNextLevelDelay);
        mPlayer.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        mPlayer.State = Player.PlayerState.ExitingLevel;
    }

    // Proceed to the next level immediately.
    public void StartNextLevelImmediate()
    {
        LevelIndex = Mathf.Min(LevelSettings.Count - 1, LevelIndex + 1);
        BuildCurrentLevel();
        SetState(State.Playing);
    }

    // Open all level exits in the scene.
    public void OpenLevelExits()
    {
        foreach (var exit in FindObjectsOfType<LevelExit>())
            exit.Open();
    }

    // Add a gameobject to the 'mObjects' list.
    public void AddToObjectsList(GameObject obj)
    {
        if (mObjects == null)
            ResetObjects();
        mObjects.Add(obj);
        obj.transform.parent = this.transform;
    }

    // Set the current game state.
    public void SetState(State state)
    {
        mState = state;
        mPlayer.enabled = mState == State.Playing;
        mPlayer.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
    }


    //-----------------------------------------------------Private Functions-----------------------------------------------------

    // Rebuild the level using the 'LevelSettings' of index 'LevelIndex'.
    private void BuildCurrentLevel(bool dontDestroyPlayer = true)
    {
        // Reseting the objects.
        ResetObjects();

        // Updating the arena.
        Arena.Calculate();

        // Setting up the player.
        if(!dontDestroyPlayer)
        {
            if(mPlayer != null)
                Destroy(mPlayer.gameObject);
            mPlayer = SpawnPrefab(PlayerPrefab.gameObject).GetComponent<Player>();
        }            
        mPlayer.transform.position = new Vector3(0.0f, mPlayer.transform.position.y, 0.0f);
        mPlayer.State = Player.PlayerState.Playing;
        mPlayer.enabled = true;

        // Spawning the enemy pods.
        for (int i = 0; i < LevelSettings[LevelIndex].EnemyPodCount; i++)
        {
            var obj = SpawnPrefabInObjectsList(EnemyPodPrefab.gameObject);
            obj.GetComponent<EnemyPod>().StartSpawnProcess((i+1) * LevelSettings[LevelIndex].TimeBetweenEnemyPods);
        }

        // Spawning the boss enemy pods.
        for (int i = 0; i < LevelSettings[LevelIndex].BossPodCount; i++)
        {
            var obj = SpawnPrefabInObjectsList(BossEnemyPodPrefab.gameObject);
            obj.GetComponent<EnemyPod>().StartSpawnProcess((i + 1) * LevelSettings[LevelIndex].TimeBetweenBossPods);
        }

        // Spawning the level exits.
        if(LevelSettings[LevelIndex].SpawnLevelExit)
            SpawnPrefabInObjectsList(LevelExitPrefab.gameObject);

        // Spawning the decals.
        for (int i = 0; i < LevelSettings[LevelIndex].DecalCount; i++)
            SpawnPrefabInObjectsList(DecalPrefab);

        // Fading in the level.
        mScreenManager.FadeIn(1.0f);

        // Enabling the cross hair.
        mScreenManager.EnableCrossHair(true);
    }

    // Spawn a prefab as a child of this object and add it to the 'mObjects' list.
    private GameObject SpawnPrefabInObjectsList(GameObject prefab)
    {
        var obj = SpawnPrefab(prefab);
        AddToObjectsList(obj);
        return obj;
    }

    // Spawn a prefab as a child of this object.
    private GameObject SpawnPrefab(GameObject prefab)
    {
        GameObject obj = Instantiate(prefab) as GameObject;
        obj.transform.parent = this.transform;
        return obj;
    }

    // Destroy all objects in the 'mObjects' list and clear it.
    private void ResetObjects()
    {
        if (mObjects != null)
        {
            for (int count = 0; count < mObjects.Count; ++count)
                Destroy(mObjects[count]);
            mObjects.Clear();
        }
        mObjects = new List<GameObject>();
    }

    // Call 'StartNextLevelImmediate()' after a delay of 'StartNextLevelDelay' seconds.
    private IEnumerator StartNextLevelAfterDelay()
    {
        yield return new WaitForSeconds(StartNextLevelDelay);
        StartNextLevelImmediate();
    }


    //-----------------------------------------------------Event Functions-----------------------------------------------------

    private void ScreenManager_OnNewGame()
    {
        StartNewGame();
    }

    private void Enemy_EnemyDied()
    {
        if (FindObjectsOfType<Enemy>().Length <= 1 && FindObjectsOfType<EnemyPod>().Length <= 0)
        {
            mScreenManager.EnableCrossHair(false);

            // Game won if all levels are complete.
            if (LevelIndex >= LevelSettings.Count - 1)
            {
                SetState(State.Paused);
                mScoreManager.ApplyMultiplier();
                mScreenManager.TransitionTo(ScreenManager.Screens.WinScreen);
            }

            // Show buff (bonus) screen if level complete.
            else
            {
                SetState(State.Paused);
                mScoreManager.ApplyMultiplier();
                mBuffManager.GenerateBuffs();
                mScreenManager.TransitionTo(ScreenManager.Screens.BuffScreen);        
            }
        }
    }

    private void Player_PlayerDied()
    {
        // Game lost if player dies.
        mScoreManager.ApplyMultiplier();
        mScreenManager.EnableCrossHair(false);
        mScreenManager.TransitionTo(ScreenManager.Screens.LooseScreen);
    }
}
