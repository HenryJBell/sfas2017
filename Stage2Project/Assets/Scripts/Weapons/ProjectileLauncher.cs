﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileLauncher : Weapon {

    [SerializeField]
    private float ShootCooldown = 0.5f;

    [SerializeField]
    private string ProjectilePath;

    [SerializeField]
    private List<Transform> ProjectileSpawns;

    private Object mProjectilePrefab;
    private float mTimeOfLastShot = 0.0f;


    //-----------------------------------------------------Unity Functions-----------------------------------------------------

    void Start()
    {
        mTimeOfLastShot = -ShootCooldown;
        mProjectilePrefab = Resources.Load(ProjectilePath);
    }


    //----------------------------------------------------Public Functions----------------------------------------------------

    // Shoot the weapon. Only characters in 'faction' will be damaged.
    public override void Activate(Faction faction)
    {
        // Returning early if the weapon has not cooled down yet.
        if (Time.time - mTimeOfLastShot < ShootCooldown) return;

        // Spawning a projectile of faction 'faction' at each spawn in 'ProjectileSpawns'.
        foreach(var spawn in ProjectileSpawns)
        {
            var projectile = Instantiate(mProjectilePrefab, spawn.position, spawn.rotation) as GameObject;
            projectile.GetComponent<Projectile>().Faction = faction;
            mTimeOfLastShot = Time.time;
        }
    }
}
