﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Projectile : MonoBehaviour {

    public float Speed = 1.0f;
    public int Damage = 1;
    public Faction Faction = Faction.Player;

    [SerializeField]
    private GameObject PlayerDamageParticle;

    [SerializeField]
    private GameObject EnemyDamageParticle;


    //-----------------------------------------------------Unity Functions-----------------------------------------------------

    void Awake()
    {
        // Adding the projectile to the 'mObjects' list in the game manager.
        var gameManager = FindObjectOfType<GameManager>();
        if(gameManager != null)
            gameManager.AddToObjectsList(this.gameObject);
    }

    void Start()
    {
        // Apply a forward velocity to the projectile.
        this.GetComponent<Rigidbody>().velocity = this.transform.TransformDirection(Vector3.forward * Speed);
    }

    void OnTriggerEnter(Collider col)
    {
        // Checking if the projectile collided with a character.
        var character = col.gameObject.GetComponent<Character>();
        if (character != null)
        {
            // Applying damage to the character if it is not the same faction as the projectile.
            if (character.Faction == this.Faction)
                return;
            character.TakeDamage(Damage);

            // Spawning damage particles.
            var particleToSpawn = character.Faction == Faction.Player ? PlayerDamageParticle : EnemyDamageParticle;
            if (particleToSpawn != null)
            {
                var particle = Instantiate(particleToSpawn, transform.position, Quaternion.identity) as GameObject;
                FindObjectOfType<GameManager>().AddToObjectsList(particle);
            }

            // Destroying the projectile.
            Destroy(this.gameObject);
        }
    }
}
