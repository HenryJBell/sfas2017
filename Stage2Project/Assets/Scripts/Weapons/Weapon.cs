﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon : MonoBehaviour {

    //-----------------------------------------------------Public Functions----------------------------------------------------

    public abstract void Activate(Faction faction);
}
