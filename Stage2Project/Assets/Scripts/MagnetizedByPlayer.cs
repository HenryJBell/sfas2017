﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class MagnetizedByPlayer : MonoBehaviour
{
    public enum Type { Attract, Repel }

    [SerializeField]
    private float RepelForce = 1000.0f;

    [SerializeField]
    private float MinimumDistance = 1.0f;

    [SerializeField]
    private Type MagnetizeType = Type.Repel;

    private Player mPlayer;
    private Rigidbody mBody;


    //-----------------------------------------------------Unity Functions-----------------------------------------------------

    void OnEnable()
    {
        mPlayer = FindObjectOfType<Player>();
        mBody = GetComponent<Rigidbody>();
    }

	void Update()
    {
        if( mPlayer != null)
        {
            // Calculating the distance between the object and the player in the direction of the force to be applied.
            Vector3 vector = (MagnetizeType == Type.Repel) ? (transform.position - mPlayer.transform.position) : (mPlayer.transform.position - transform.position);

            if(vector.magnitude <= MinimumDistance)
                mBody.AddForce(vector.normalized * RepelForce * Time.deltaTime / (vector.magnitude / MinimumDistance));
        }
	}
}
