﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// A set of different buffs to be used on the bonus screen.
// These buffs are added to 'mBuffLibrary' in the buff manager.


//-----------------------------------------------------Abstract Buffs-----------------------------------------------------

public abstract class Buff
{
    public string ButtonTitle;
    public string ButtonImagePath;
    public string ButtonDescription;

    public abstract void Activate();
}

public abstract class HPBuff : Buff
{
    public int HealValue = 0;

    public override void Activate()
    {
        var player = GameObject.FindObjectOfType<Player>();
        if (player != null)
        {
            player.Heal(HealValue);
            player.SetWeaponConfig(Player.WeaponConfig.Default);
        }
    }
}

public abstract class WeaponBuff : Buff
{
    public Player.WeaponConfig WeaponConfig;

    public override void Activate()
    {
        var player = GameObject.FindObjectOfType<Player>();
        if (player != null)
            player.SetWeaponConfig(WeaponConfig);
    }
}


//-----------------------------------------------------Usable Buffs-----------------------------------------------------

public class Buff_DualWeaponConfig : WeaponBuff
{
    public Buff_DualWeaponConfig()
    {
        ButtonTitle = "DUAL CANNONS";
        ButtonImagePath = "BuffSprites/Tex_Buff_DualWeapons";
        ButtonDescription = "DOUBLE UP ON YOUR LASER CANNONS FOR ONE LEVEL";
        WeaponConfig = Player.WeaponConfig.Dual;
    }
}

public class Buff_ShotgunWeaponConfig : WeaponBuff
{
    public Buff_ShotgunWeaponConfig()
    {
        ButtonTitle = "LASER SHOTGUN";
        ButtonImagePath = "BuffSprites/Tex_Buff_ShotgunWeapons";
        ButtonDescription = "UPGRADE TO A TRI LASER SHOTGUN FOR ONE LEVEL";
        WeaponConfig = Player.WeaponConfig.Shotgun;
    }
}

public class Buff_2HP : HPBuff {

    public Buff_2HP()
    {
        ButtonTitle = "REPAIRS";
        ButtonImagePath = "BuffSprites/Tex_Buff_Spanner";
        ButtonDescription = "RECOVER 2 POINTS OF HEALTH";
        HealValue = 2;
    }    
}

public class Buff_4HP : HPBuff
{
    public Buff_4HP()
    {
        ButtonTitle = "DOUBLE REPAIRS";
        ButtonImagePath = "BuffSprites/Tex_Buff_DoubleSpanner";
        ButtonDescription = "RECOVER 4 POINTS OF HEALTH";
        HealValue = 4;
    }
}