﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class BuffButton
{
    public Text ButtonTitle;
    public Image ButtonImage;
    public Text ButtonDescription;
    public int BuffIndex;
}

public class BuffManager : MonoBehaviour
{
    [SerializeField]
    private List<BuffButton> BuffButtons;

    private List<Buff> mBuffLibrary;
    private ScreenManager mScreenManager;


    //-----------------------------------------------------Unity Functions-----------------------------------------------------

    void Awake()
    {
        mScreenManager = FindObjectOfType<ScreenManager>();
        PopulateBuffLibrary();
    }


    //-----------------------------------------------------Public Functions-----------------------------------------------------

    // Activate buff button 1s buff and close the buff screen.
    public void Buff1Chosen()
    {
        mBuffLibrary[BuffButtons[0].BuffIndex].Activate();
        CloseBuffScreen();
    }

    // Activate buff button 2s buff and close the buff screen.
    public void Buff2Chosen()
    {
        mBuffLibrary[BuffButtons[1].BuffIndex].Activate();
        CloseBuffScreen();
    }

    // Generate random unique buffs for the buff buttons on the bonus screen.
    public void GenerateBuffs()
    {
        // Creating a list of available buffs.
        List<int> availableBuffIndexes = new List<int>();
        for (int i = 0; i < mBuffLibrary.Count; i++)
            availableBuffIndexes.Add(i);

        // Looping through the buff buttons.
        for(int i = 0; i < BuffButtons.Count; i++)
        {
            // Chosing a random buff from those available, applying it to the button and making it no longer available.
            int index = Random.Range(0, availableBuffIndexes.Count);
            BuffButtons[i].BuffIndex = availableBuffIndexes[index];
            availableBuffIndexes.RemoveAt(index);

            // Updating the various button properties.
            BuffButtons[i].ButtonTitle.text = mBuffLibrary[BuffButtons[i].BuffIndex].ButtonTitle;
            BuffButtons[i].ButtonImage.sprite = Resources.Load<Sprite>(mBuffLibrary[BuffButtons[i].BuffIndex].ButtonImagePath);
            BuffButtons[i].ButtonDescription.text = mBuffLibrary[BuffButtons[i].BuffIndex].ButtonDescription;
        }
    }


    //-----------------------------------------------------Private Functions-----------------------------------------------------

    // Populate 'mBuffLibrary' with all usable buffs.
    private void PopulateBuffLibrary()
    {
        mBuffLibrary = new List<Buff>();
        mBuffLibrary.Add(new Buff_2HP());
        mBuffLibrary.Add(new Buff_4HP());
        mBuffLibrary.Add(new Buff_DualWeaponConfig());
        mBuffLibrary.Add(new Buff_ShotgunWeaponConfig());
    }

    // Close the buff screen and transition to the game screen.
    private void CloseBuffScreen()
    {
        mScreenManager.TransitionTo(ScreenManager.Screens.GameScreen);
        mScreenManager.EnableCrossHair(true);

        var gameManager = FindObjectOfType<GameManager>();
        if(gameManager != null)
        {
            gameManager.OpenLevelExits();
            gameManager.SetState(GameManager.State.Playing);
        }        
    }
}
