﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RawImage))]
public class ScrollingRawImage : MonoBehaviour {

    [SerializeField]
    private Vector2 ScrollVector;

    private RawImage mImage;


    //----------------------------------------------------Unity Functions---------------------------------------------------

    void Start()
    {
        mImage = GetComponent<RawImage>();
    }

    void Update()
    {
        // Scrolling 'mImage' by 'ScrollVector' per second.
        var uvRect = mImage.uvRect;
        uvRect.position += ScrollVector * Time.deltaTime;
        mImage.uvRect = uvRect;
    }
}
