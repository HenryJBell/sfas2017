﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(Animation))]
public class LevelExit : MonoBehaviour {

    [SerializeField]
    private float OpenDelay = 1.0f;

    private bool mOpen = false;
    private GameManager mGameManager;


    //-----------------------------------------------------Unity Functions-----------------------------------------------------

    void Awake()
    {
        mGameManager = FindObjectOfType<GameManager>();
        mOpen = false;
    }

    void OnTriggerStay(Collider col)
    {
        // Starting the next level if the player is on the open exit.
        if (col.gameObject.GetComponent<Player>() && mOpen)
        {
            mGameManager.StartNextLevel();
            mOpen = false;
        }
    }


    //-----------------------------------------------------Public Functions-----------------------------------------------------

    // Open the level exit after a delay of 'OpenDelay' seconds.
    public void Open()
    {
        GetComponent<Animation>().Play("Anim_LevelExitOpen");
        StartCoroutine(OpenAfterDelay());
    }


    //-----------------------------------------------------Private Functions-----------------------------------------------------

    // Set 'mOpen' to true after 'OpenDelay' seconds.
    private IEnumerator OpenAfterDelay()
    {
        yield return new WaitForSeconds(OpenDelay);
        mOpen = true;
    }
}
